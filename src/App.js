import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'

class App extends Component {

  constructor(props) {
    super(props)
    this.state = {
      counter: 0,
      error: false
    }
  }

  increment() {
    this.setState({
      counter: this.state.counter + 1,
      error: false
    })
  }

  decrement() {
    if (this.state.counter === 0) {
      this.setState({
        error: true
      })
    } else {
      this.setState({
        counter: this.state.counter - 1
      })
    }
  }

  render() {
    return (
      <div data-test="component-app">
        <h1 data-test="counter-display">
          The counter is currently {this.state.counter}
        </h1>
        {this.state.error &&
          <h2 data-test="error">The counter cannot go below 0</h2>
        }
        <button data-test="increment-button"
                onClick={() => this.increment()}>
          Increment counter
        </button>
        <button data-test="decrement-button"
                onClick={() => this.decrement()}>
          Decrement counter
        </button>
      </div>
    )
  }
}

export default App
